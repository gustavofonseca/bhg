import unittest
from bhg.bhg import MetaCard
from bhg.bhg import CardField
from bhg.fieldtypes import TextField
from bhg.ordered import OrderedModel

class TestMetaCardDefinition(unittest.TestCase):
    def setUp(self):
        self.book_metadata = MetaCard('Book')

    def test_card_id(self):
        self.assertEqual(self.book_metadata.name, 'Book')

    def test_add_field(self):
        self.book_metadata.add_field('title', TextField)
        self.assertTrue(isinstance(self.book_metadata['title'], CardField))

    def test_contains_field(self):
        self.book_metadata.add_field('title', TextField)
        self.assertTrue('title' in self.book_metadata)

class TestCard(unittest.TestCase):
    def setUp(self):
        self.book_metadata = MetaCard('Book')
        self.book_metadata.add_field('title', TextField)
        self.BookCard = self.book_metadata.materialize()

    def test_card_creation(self):
        self.assertTrue(issubclass(self.BookCard, OrderedModel))

    def test_contains_attr(self):
        Book = self.BookCard()
        self.assertTrue('title' in Book)

    def test_valid_value(self):
        Book = self.BookCard()
        Book.title = 'Le Parfum'
        self.assertEquals(Book.title, 'Le Parfum')

    def test_invalid_value(self):
        Book = self.BookCard()
        self.assertRaises(TypeError, setattr, Book, 'title', ['Le Parfum', ])

