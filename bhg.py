# coding: utf-8
from ordered import OrderedModel

class CardField(object):
    def __init__(self, name, field_type):
        self.name = name
        self.field_type = field_type

    def __unicode__(self):
        return self.name

class CardFieldsList(list):
    def __init__(self, *args):
        super(CardFieldsList, self).__init__(*args)
        self.__field_order = {}

    def append(self, item):
        super(CardFieldsList, self).append(item)
        self.__field_order[item.name] = self.index(item)

    def get_by_name(self, name):
        return self[self.__field_order[name]]

class MetaCard(object):
    def __init__(self, name):
        self.name = name
        self.__fields = CardFieldsList()

    def add_field(self, name, field_type):
        if name in self.__fields:
            raise ValueError("Field '%s' already exists" % name)

        card_field = CardField(name, field_type)
        self.__fields.append(card_field)

    def __iter__(self):
        return (field.name for field in self.__fields)

    def __getitem__(self, key):
        if key not in self:
            raise KeyError()

        return self.__fields.get_by_name(key)

    def materialize(self):
        return type(self.name, (OrderedModel,), dict((field.name, field.field_type()) for field in self.__fields))

