# coding: utf-8
from ordered import OrderedProperty

class TextField(OrderedProperty):
    def __set__(self, instance, value):
        if not isinstance(value, basestring):
            raise TypeError()
            
        super(TextField, self).__set__(instance, value)

    

